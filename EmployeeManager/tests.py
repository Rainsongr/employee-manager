from django.test import TestCase
from django.urls import reverse
from rest_framework.test import APIClient
from rest_framework import status

from .models import Employee


class EmployeesModelTestCase(TestCase):
    """This class defines the test suite for the Employee model."""

    def setUp(self):
        """Define the test client and other test variables."""
        self.employee = Employee(
            name='Albert Einstein',
            email='albert@einstein.com'
        )

    def test_can_create_employee(self):
        """Test the employee model can create a employee."""
        old_count = Employee.objects.count()
        self.employee.save()
        new_count = Employee.objects.count()
        self.assertEqual(new_count, old_count + 1)


class EmployeeViewTestCase(TestCase):
    """Test suite for the api views."""

    def setUp(self):
        """Define the test client and other test variables."""
        self.client = APIClient()
        self.payload = {'name': 'Alan Turing', 'email': 'alan@turing.com'}
        Employee(
            name='Albert Einstein',
            email='albert@einstein.com'
        ).save()

    def test_list(self):
        """Test employee listing."""
        employees = Employee.objects.get()

        response = self.client.get(
            reverse('employee_list'),
            format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, employees)

    def test_detail(self):
        """Test the api can get a given employee."""
        employee = Employee.objects.get()
        response = self.client.get(
            reverse('employee_detail',
                    kwargs={'pk': employee.id}), format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, employee)

    def test_post(self):
        """Test Employee POST."""
        old_count = Employee.objects.count()
        response = self.client.post(reverse('employee_list'),
                                    {"name": "Alan Turing", "email": "alan@turing.com"})
        new_count = Employee.objects.count()
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(new_count, old_count + 1)
        pass

    def test_update(self):
        """Test Employee POST."""
        # self.assertEqual(self.response.status_code, status.HTTP_201_CREATED)
        pass

    def test_delete(self):
        """Test the api can delete a employee."""
        employee = Employee.objects.get()
        response = self.client.delete(
            reverse('employee_detail', kwargs={'pk': employee.id}),
            format='json',
            follow=True)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
