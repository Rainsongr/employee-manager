from django.contrib import admin

from .models import Employee


class EmployeeAdmin(admin.AdminSite):
    site_title = 'Luiza Labs Apps'

    site_header = 'Luiza Labs Apps'

    index_title = 'Registered Apps'


admin_site = EmployeeAdmin()
admin_site.register(Employee)
