from django.urls import path, include

from .views import EmployeeList, EmployeeDetail
from .admin import admin_site
urlpatterns = [
    path('admin/', admin_site.urls),

    path('employee/', EmployeeList.as_view(), name="employee_list"),
    path('employee/<int:pk>', EmployeeDetail.as_view(), name="employee_detail")
]