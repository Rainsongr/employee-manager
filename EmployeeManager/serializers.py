from rest_framework import serializers
from .models import Employee

class EmployeeSerializer(serializers.ModelSerializer):
    """Serializer to map the Model instance into JSON format."""

    class Meta:
        """Meta class to map serializer's fields with the model fields."""
        model = Employee
        fields = ('id', 'name', 'email')
        read_only_fields = ('date_created', 'date_modified')
