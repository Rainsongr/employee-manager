# Luizalabs Employee Manager

Django app for managing Luiza Labs's Employees.

## Admin Panel
### /admin

## Employee API
#### /employee/
- GET
    - List all employees.
- POST
    - Create a new Employee
    - payload format: 
        `{"name": "Albert", "email": "alb@ert.com", "department": "HR"}`

#### /employee/:id
- GET
    - Returns a single employee.
- PATCH
    - Update an Employee.
    - payload format: 
        `{"name": "Albert", "email": "alb@ert.com", "department": "HR"}`
- DELETE
    - Delete an Employee.

## Getting Started

### Prerequisites

Python 3.4+


### Installing

Setup environment

```
$ virtualenv --no-site-packages env

$ source env/bin/activate

$ pip install -r requirements.txt
```

Setup database

```
$ ./manage.py migrate

$ ./manage.py createsuperuser
```

Start Server

```
$ ./manage.py runserver
```

## Running the tests

```
$ ./manage.py test
```